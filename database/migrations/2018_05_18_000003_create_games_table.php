<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'games';

    /**
     * Run the migrations.
     * @table games
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id')->unique();
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('url')->nullable();
            $table->mediumText('summary')->nullable();
            $table->string('franchise')->nullable();
            $table->double('rating')->nullable();
            $table->string('first_release_date')->nullable();
            $table->integer('rating_count')->nullable();
            $table->string('cover')->nullable();
            $table->json('games')->nullable();
            $table->json('developers')->nullable();
            $table->json('release_dates')->nullable();
            $table->json('publishers')->nullable();
            $table->json('game_engines')->nullable();
            $table->json('player_perspectives')->nullable();
            $table->json('game_modes')->nullable();
            $table->json('themes')->nullable();
            $table->json('genres')->nullable();
            $table->json('dlcs')->nullable();
            $table->json('platforms')->nullable();
            $table->json('alternative_names')->nullable();
            $table->json('screenshots')->nullable();
            $table->json('websites')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
