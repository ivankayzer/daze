<div class="col-12 col-sm-6 col-md-3">
    <div class="card card-lg m-x-10">
        <div class="card-img">
            <a href="{{ $game->getLink() }}">
                <img src="{{ $game->getCover()->getScreenshotMed() }}" class="card-img-top" onerror="this.src='{{ asset('img/placeholder-image.png') }}'" alt="{{ $game->getName() }}">
            </a>
        </div>
        <div class="card-block">
            <h4 class="card-title">
                <a href="{{ $game->getLink() }}">{{ $game->getName() }}</a>
            </h4>
            <div class="card-meta">
                <span>{{ $game->getReleaseDate() }}</span>
            </div>
            <p class="card-text">{{ $game->getShortSummary() }}</p>
        </div>
    </div>
</div>
