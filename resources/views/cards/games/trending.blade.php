<div class="card card-review">
    <a class="card-img" href="{{ route('games.show') }}">
        <img src="{{ $game->getCover()->getCoverBig() }}" alt="{{ $game->getName() }}">
        <div class="badge badge-success">{{ $game->getRating() }}</div>
    </a>
    <div class="card-block">
        <h4 class="card-title"><a href="{{ route('games.show') }}">{{ $game->getName() }}</a></h4>
        <p>{{ $game->getShortSummary(75) }}</p>
    </div>
</div>