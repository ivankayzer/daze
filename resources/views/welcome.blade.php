@extends('layouts.app')

@section('styles')
    <link href="{{ asset('plugins/owl-carousel/css/owl.carousel.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section class="p-y-5">
        <div class="owl-carousel owl-posts">
            <div class="post-carousel">
                <a href="blog-post.html"><img src="https://img.youtube.com/vi/82Xd-lHmtzk/maxresdefault.jpg" alt=""></a>
                <span class="badge badge-ps4">ps4</span>
                <div class="post-block">
                    <div class="post-caption">
                        <h2 class="post-title">
                            <a href="blog-post.html">World of Tanks Campaign Hands-On: It's Got War Stories to Tell</a>
                        </h2>
                        <div class="post-meta">
                            <span><i class="fa fa-clock-o"></i> Julye 28, 2017 by <a href="profile.html">Venom</a></span>
                            <span><a href="blog-post.html#comments"><i class="fa fa-comment-o"></i> 98 comments</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-carousel">
                <a href="blog-post.html"><img src="https://img.youtube.com/vi/BlINFsdTKQ4/maxresdefault.jpg" alt=""></a>
                <span class="badge badge-xbox-one">xbox one</span>
                <div class="post-block">
                    <div class="post-caption">
                        <h2 class="post-title">
                            <a href="blog-post.html">Monster Hunter World Is the Best Introduction to the Series</a>
                        </h2>
                        <div class="post-meta">
                            <span><i class="fa fa-clock-o"></i> Julye 25, 2017 by <a href="profile.html">Lobo</a></span>
                            <span><a href="blog-post.html#comments"><i class="fa fa-comment-o"></i> 13 comments</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-carousel">
                <a href="blog-post.html"><img src="https://img.youtube.com/vi/SP8ZN7uXJ10/maxresdefault.jpg" alt=""></a>
                <span class="badge badge-steam">steam</span>
                <div class="post-block">
                    <div class="post-caption">
                        <h2 class="post-title">
                            <a href="blog-post.html">For Honor: Every Highlander Execution and Emote</a>
                        </h2>
                        <div class="post-meta">
                            <span><i class="fa fa-clock-o"></i> Julye 20, 2017 by <a href="profile.html">YAKUZI</a></span>
                            <span><a href="blog-post.html#comments"><i class="fa fa-comment-o"></i> 7 comments</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-secondary p-t-15 p-b-5 p-x-15">
        <div class="owl-carousel owl-videos">
            <div class="card card-video">
                <div class="card-img">
                    <a href="video-post.html">
                        <img src="https://i1.ytimg.com/vi/GaERL8Nrl9k/mqdefault.jpg" alt="Tom Clancy's Ghost Recon: Wildlands">
                    </a>
                    <div class="card-meta">
                        <span>4:32</span>
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title"><a href="video-post.html">Tom Clancy's Ghost Recon: Wildlands</a></h4>
                    <div class="card-meta">
                        <span><i class="fa fa-clock-o"></i> 2 hours ago</span>
                        <span>423 views</span>
                    </div>
                </div>
            </div>

            <div class="card card-video">
                <div class="card-img">
                    <a href="video-post.html">
                        <img src="https://i1.ytimg.com/vi/mW4LMCtoIkg/mqdefault.jpg" class="card-img-top" alt="Anthem Official Gameplay Reveal">
                    </a>
                    <div class="card-meta">
                        <span>6:46</span>
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title"><a href="video-post.html">Anthem Official Gameplay Reveal</a></h4>
                    <div class="card-meta">
                        <span><i class="fa fa-clock-o"></i> 2 weeks ago</span>
                        <span>447 views</span>
                    </div>
                </div>
            </div>

            <div class="card card-video">
                <div class="card-img">
                    <a href="video-post.html">
                        <img src="https://i1.ytimg.com/vi/-PohBqV_i7s/mqdefault.jpg" class="card-img-top" alt="Shadow of War Gameplay Walkthrough">
                    </a>
                    <div class="card-meta">
                        <span>9:58</span>
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title"><a href="video-post.html">Shadow of War Gameplay Walkthrough</a></h4>
                    <div class="card-meta">
                        <span><i class="fa fa-clock-o"></i> March 10, 2017</span>
                        <span>914 views</span>
                    </div>
                </div>
            </div>

            <div class="card card-video">
                <div class="card-img">
                    <a href="video-post.html">
                        <img src="https://i1.ytimg.com/vi/feqIj5PaqCQ/mqdefault.jpg" class="card-img-top" alt="Call of Duty WW2 Multiplayer Gameplay">
                    </a>
                    <div class="card-meta">
                        <span>4:32</span>
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title"><a href="video-post.html">Call of Duty WW2 Multiplayer Gameplay</a></h4>
                    <div class="card-meta">
                        <span><i class="fa fa-clock-o"></i> 3 days ago</span>
                        <span>423 views</span>
                    </div>
                </div>
            </div>

            <div class="card card-video">
                <div class="card-img">
                    <a href="video-post.html">
                        <img src="https://i.ytimg.com/vi/N1NsF9c90f0/mqdefault.jpg" alt="Final Fantasy VII Remake">
                    </a>
                    <div class="card-meta">
                        <span>3:05</span>
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title"><a href="video-post.html">Final Fantasy VII Remake</a></h4>
                    <div class="card-meta">
                        <span><i class="fa fa-clock-o"></i> 2 days ago</span>
                        <span>589 views</span>
                    </div>
                </div>
            </div>

            <div class="card card-video">
                <div class="card-img">
                    <a href="video-post.html">
                        <img src="https://i.ytimg.com/vi/lQXpDL3SNWQ/mqdefault.jpg" class="card-img-top" alt="Spider-Man Official 4K Trailer">
                    </a>
                    <div class="card-meta">
                        <span>3:07</span>
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title"><a href="video-post.html">Spider-Man Official 4K Trailer</a></h4>
                    <div class="card-meta">
                        <span><i class="fa fa-clock-o"></i> 2 weeks ago</span>
                        <span>1798 views</span>
                    </div>
                </div>
            </div>

            <div class="card card-video">
                <div class="card-img">
                    <a href="video-post.html">
                        <img src="https://i1.ytimg.com/vi/9EzRBzdf--g/mqdefault.jpg" alt="METRO EXODUS Gameplay Trailer">
                    </a>
                    <div class="card-meta">
                        <span>1:24</span>
                    </div>
                </div>
                <div class="card-block">
                    <h4 class="card-title"><a href="video-post.html">Wolfenstein II Gameplay Trailer</a></h4>
                    <div class="card-meta">
                        <span><i class="fa fa-clock-o"></i> July 16, 2017</span>
                        <span>7330 views</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 offset-lg-1">
                <section class="p-y-80">
                    <div class="container widget">
                            <h5 class="widget-title">Recent Games</h5>
                        <div class="row">
                            @foreach($recent->getGames() as $game)
                                @include('cards.games.home', ['game' => $game])
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-3">
                <div class="sidebar p-y-80">
                    <!-- widget-games -->
                    <div class="widget widget-games">
                        <h5 class="widget-title">Upcoming Games</h5>
                        <a href="#" style="background-image: url('https://i1.ytimg.com/vi/mW4LMCtoIkg/mqdefault.jpg')">
                            <span class="overlay"></span>
                            <div class="widget-block">
                                <div class="count">1</div>
                                <div class="description">
                                    <h5 class="title">Horizon: Zero Dawn The Frozen Wilds</h5>
                                    <span class="date">November 14, 2017</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" style="background-image: url('https://i1.ytimg.com/vi/GaERL8Nrl9k/mqdefault.jpg')">
                            <span class="overlay"></span>
                            <div class="widget-block">
                                <div class="count">2</div>
                                <div class="description">
                                    <h5 class="title">Tom Clancy's Ghost Recon: Wildlands</h5>
                                    <span class="date">August 29, 2017</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" style="background-image: url('https://i1.ytimg.com/vi/feqIj5PaqCQ/mqdefault.jpg')">
                            <span class="overlay"></span>
                            <div class="widget-block">
                                <div class="count">3</div>
                                <div class="description">
                                    <h5 class="title">Call of Duty WW2</h5>
                                    <span class="date">December 15, 2017</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" style="background-image: url('https://i.ytimg.com/vi/N1NsF9c90f0/mqdefault.jpg')">
                            <span class="overlay"></span>
                            <div class="widget-block">
                                <div class="count">4</div>
                                <div class="description">
                                    <h5 class="title">Final Fantasy VII</h5>
                                    <span class="date">Q3 2018</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" style="background-image: url('https://i1.ytimg.com/vi/xUGRjNzGz3o/mqdefault.jpg')">
                            <span class="overlay"></span>
                            <div class="widget-block">
                                <div class="count">5</div>
                                <div class="description">
                                    <h5 class="title">Mass Effect Andromeda</h5>
                                    <span class="date">Q1, 2018</span>
                                </div>
                            </div>
                        </a>
                    </div>

                    <!-- widget post  -->
                    <div class="widget widget-post">
                        <h5 class="widget-title">Recommended</h5>
                        <a href="blog-post.html"><img src="https://i1.ytimg.com/vi/4BLkEJu9szM/mqdefault.jpg" alt=""></a>
                        <h4><a href="blog-post.html">Titanfall 2's Trophies Only Have 3 Multiplayer</a></h4>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <section class="hero" style="background-image: url('img/hero/hero.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="hero-block">
                <h2 class="hero-title">The Witcher 3: Don't Miss This Hidden Contract</h2>
                <p>The world is in chaos. The air is thick with tension and the smoke of burnt villages.</p>
                <a class="btn btn-primary btn-lg btn-shadow btn-md btn-rounded" href="blog-post.html" role="button">Read More</a>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/owl-carousel/js/owl.carousel.min.js') }}"></script>
    <script>
        (function($) {
            "use strict";
            $('.owl-posts').owlCarousel({
                margin: 5,
                loop: true,
                dots: false,
                autoplay: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    1024: {
                        items: 1,
                        center: false
                    },
                    1200: {
                        items: 2,
                        center: true
                    }
                }
            });

            $('.owl-videos').owlCarousel({
                margin: 15,
                loop: true,
                dots: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    700: {
                        items: 2
                    },
                    800: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    },
                    1200: {
                        items: 6
                    }
                }
            });
        })(jQuery);
    </script>
@endsection
