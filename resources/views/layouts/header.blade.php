<header id="header">
    <div class="container">
        <div class="navbar-backdrop">
            <div class="navbar">
                <div class="navbar-left">
                    <a class="navbar-toggle"><i class="fa fa-bars"></i></a>
                    <a href="{{ route('welcome') }}" class="logo"><img src="{{ asset('img/logo.png') }}" alt="Daze"></a>
                    <nav class="nav">
                        <ul>
                            <li><a href="{{ route('welcome') }}">Home</a></li>
                            <li><a href="{{ route('news.index') }}">News</a></li>
                            <li class="has-dropdown mega-menu mega-games">
                                <a href="{{ route('games.index') }}">Games</a>
                                <ul>
                                    <li>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="img">
                                                        <a href="{{ route('games.show') }}"><img src="{{ asset('img/menu/menu-1.jpg') }}"
                                                                                      alt="Last of Us: Part 2"></a>
                                                        <span class="badge badge-ps4">PS4</span>
                                                    </div>
                                                    <h4><a href="{{ route('games.show') }}">Grand Theft Auto V</a></h4>
                                                    <span>Jun 29, 2019</span>
                                                </div>
                                                <div class="col">
                                                    <div class="img">
                                                        <a href="{{ route('games.show') }}"><img src="{{ asset('img/menu/menu-1.jpg') }}"
                                                                                      alt="Injustice 2"></a>
                                                        <span class="badge badge-steam">Steam</span>
                                                    </div>
                                                    <h4><a href="{{ route('games.show') }}">Injustice 2</a></h4>
                                                    <span>June 10, 2017</span>
                                                </div>
                                                <div class="col">
                                                    <div class="img">
                                                        <a href="{{ route('games.show') }}"><img src="{{ asset('img/menu/menu-1.jpg') }}"
                                                                                      alt="Bioshock: Infinite"></a>
                                                        <span class="badge badge-xbox-one">Xbox One</span>
                                                    </div>
                                                    <h4><a href="{{ route('games.show') }}">Bioshock: Infinite</a></h4>
                                                    <span>May 16, 2017</span>
                                                </div>
                                                <div class="col">
                                                    <div class="img">
                                                        <a href="{{ route('games.show') }}"><img src="{{ asset('img/menu/menu-1.jpg') }}"
                                                                                      alt="Batman: Arkham Knight"></a>
                                                        <span class="badge badge-ps4">PS4</span>
                                                    </div>
                                                    <h4><a href="{{ route('games.show') }}">Batman: Arkham Knight</a></h4>
                                                    <span>May 10, 2017</span>
                                                </div>
                                                <div class="col">
                                                    <div class="img">
                                                        <a href="{{ route('games.show') }}"><img src="{{ asset('img/menu/menu-1.jpg') }}"
                                                                                      alt="Bioshock: Infinite"></a>
                                                        <span class="badge badge-pc">PC</span>
                                                    </div>
                                                    <h4><a href="{{ route('games.show') }}">Hitman Absolution</a></h4>
                                                    <span>May 10, 2017</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            <li class="has-dropdown mega-menu">
                                <a href="#">Available pages</a>
                                <ul>
                                    <li>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col">
                                                    <a href="{{ route('login') }}">Login</a>
                                                    <a href="{{ route('register') }}">Register</a>
                                                    <a href="{{ route('welcome') }}">Home</a>
                                                    <a href="{{ route('news.index') }}">News: Index</a>
                                                    <a href="{{ route('news.show') }}">News: Single</a>
                                                    <a href="{{ route('users.profile.timeline') }}">Profile:
                                                        Timeline</a>
                                                    <a href="{{ route('users.profile.images') }}">Profile: Images</a>
                                                    <a href="{{ route('users.profile.collections') }}">Profile: Collections</a>
                                                    <a href="{{ route('games.index') }}">Games: Images</a>
                                                    <a href="{{ route('games.show') }}">Games: Single</a>
                                                    <a href="404">404</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                @guest
                    <div class="nav navbar-right">
                        @auth
                            <ul>
                                <li class="dropdown dropdown-profile">
                                    <a href="login.html" data-toggle="dropdown"><img src="img/user/avatar-sm.jpg"
                                                                                     alt="">
                                        <span>Nathan Drake</span></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item active" href="#"><i class="fa fa-user"></i> Profile</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-envelope-open"></i> Inbox</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-heart"></i> Games</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-cog"></i> Settings</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="login.html"><i class="fa fa-sign-out"></i> Logout</a>
                                    </div>
                                </li>
                                <li class="dropdown dropdown-notification">
                                    <a href="register.html" data-toggle="dropdown">
                                        <i class="fa fa-bell"></i>
                                        <span class="badge badge-danger">2</span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <h5 class="dropdown-header"><i class="fa fa-bell"></i> Notifications</h5>
                                        <div class="dropdown-block">
                                            <a class="dropdown-item" href="#">
                                                <span class="badge badge-info"><i
                                                            class="fa fa-envelope-open"></i></span> new email
                                                <span class="date">just now</span>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge badge-danger"><i class="fa fa-thumbs-up"></i></span>
                                                liked your post
                                                <span class="date">5 mins</span>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge badge-primary"><i class="fa fa-user-plus"></i></span>
                                                friend request
                                                <span class="date">2 hours</span>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge badge-info"><i class="fa fa-envelope"></i></span> new
                                                email
                                                <span class="date">3 days</span>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge badge-info"><i class="fa fa-video-camera"></i></span>
                                                sent a video
                                                <span class="date">5 days</span>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge badge-danger"><i class="fa fa-thumbs-up"></i></span>
                                                liked your post
                                                <span class="date">8 days</span>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li><a data-toggle="search"><i class="fa fa-search"></i></a></li>
                            </ul>
                        @else
                            <ul>
                                <li class="hidden-xs-down"><a href="{{ route('login') }}">Login</a></li>
                                <li class="hidden-xs-down"><a href="{{ route('register') }}">Register</a></li>
                                <li><a data-toggle="search"><i class="fa fa-search"></i></a></li>
                            </ul>
                        @endauth
                    </div>
                @else
                    <div class="nav navbar-right">
                        <ul>
                            <li class="dropdown dropdown-profile">
                                <a href="login.html" data-toggle="dropdown"><img src="img/user/avatar-sm.jpg" alt="">
                                    <span>Nathan Drake</span></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item active" href="#"><i class="fa fa-user"></i> Profile</a>
                                    <a class="dropdown-item" href="#"><i class="fa fa-cog"></i> Settings</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="login.html"><i class="fa fa-sign-out"></i> Logout</a>
                                </div>
                            </li>
                            <li><a data-toggle="search"><i class="fa fa-search"></i></a></li>
                        </ul>

                    </div>
                @endguest
            </div>
        </div>
        <div class="navbar-search">
            <div class="container">
                <form action="{{ route('search.index') }}" method="get">
                    <input type="text" class="form-control" name="term" placeholder="Search...">
                    <i class="fa fa-times close"></i>
                </form>
            </div>
        </div>
    </div>
</header>
