<footer id="footer">
    <div class="container">
        <div class="footer-bottom">
            <p>Copyright &copy; {{ date("Y") }} Daze. All rights reserved.</p>
        </div>
    </div>
</footer>
