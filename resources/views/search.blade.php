@extends('layouts.app')

@section('content')
    <section class="p-x-100">
        <div class="container-fluid">
            <div class="toolbar-custom">
                <form method="get" action="{{ route('search.index') }}" class="col-sm-12">
                    <div class="form-group input-icon-right">
                        <i class="fa fa-search"></i>
                        <input type="text" name="term" value="{{ $term }}" class="form-control search-game" placeholder="Search Game...">
                    </div>
                </form>
            </div>

            <div class="row">
                @foreach($results->getGames() as $game)
                    @include('cards.games.search', ['game' => $game])
                @endforeach
            </div>
        </div>
    </section>
@endsection