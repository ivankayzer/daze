@extends('layouts.app')

@section('styles')
    <link href="{{ asset('plugins/owl-carousel/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{ asset('plugins/owl-carousel/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('plugins/lightbox/lightbox.js') }}"></script>
    <script src="{{ asset('plugins/sticky/jquery.sticky.js') }}"></script>
    <script src="{{ asset('plugins/easypiechart/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('plugins/easypiechart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script>
        $('.select2').select2({
            placeholder: "Select collection"
        });

        (function ($) {
            "use strict";
            $('.owl-carousel').owlCarousel({
                margin: 15,
                loop: true,
                dots: false,
                autoplay: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    700: {
                        items: 2
                    },
                    800: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    },
                    1200: {
                        items: 6
                    }
                }
            });
        })(jQuery);
    </script>
    <script>
        (function ($) {
            "use strict";
            $('.chart').easyPieChart({
                barColor: '#5eb404',
                trackColor: '#e3e3e3',
                easing: 'easeOutBounce',
                onStep: function (from, to, percent) {
                    $(this.el).find('span').text(Math.round(percent));
                }
            });
        })(jQuery);

        (function ($) {
            "use strict";
            $('[data-lightbox]').lightbox({})
        })(jQuery);
    </script>
@endsection

@section('content')
    <section class="hero hero-game" style="background-image: url('{{ asset('img/hero/hero.jpg') }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="hero-block">
                <div class="hero-left">
                    <h2 class="hero-title p-y-25">The Witcher 3: Wild Hunt</h2>
                    <select class="select2 form-control">
                        <option></option>
                        <option value="ps4">Playstation 4</option>
                        <option value="xbox-one">Xbox One</option>
                        <option value="steam">Steam</option>
                        <option value="origin">Origin</option>
                        <option value="uplay">Uplay</option>
                    </select>
                </div>
                <div class="hero-right">
                    <div class="hero-review">
                        <span>IGDB</span>
                        <div class="chart easypiechart" data-percent="89"
                           data-scale-color="#e3e3e3"><span>89</span>%</div>
                    </div>
                    <div class="hero-review">
                        <span>Metacritic</span>
                        <a href="http://www.metacritic.com/game/playstation-4/god-of-war">93</a>
                    </div>
                    <div class="hero-review">
                        <span>Users</span>
                        <a href="http://www.metacritic.com/game/playstation-4/god-of-war">9.1</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="p-y-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <section class="bg-image">
                        <div class="container" style="width: 900px;">
                            <div class="video-play"
                                 data-src="https://www.youtube.com/embed/i8qzBkHjk_0?rel=0&amp;amp;autoplay=1&amp;amp;showinfo=0">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <img class="embed-responsive-item"
                                         src="https://img.youtube.com/vi/K5tRSwd-Sc0/maxresdefault.jpg">
                                    <div class="video-caption">
                                        <h5>For Honor: Walkthrough Gameplay Warlords</h5>
                                        <span class="length">5:32</span>
                                    </div>
                                    <div class="video-play-icon">
                                        <i class="fa fa-play"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-lg-3">
                    <!-- sidebar -->
                    <div class="sidebar p-t-75 mh-100">
                        <!-- widget game -->
                        <div class="widget widget-game mh-100">
                            <div class="widget-block"
                                 style="background-image: url('https://img.youtube.com/vi/U75Qkzc2tqA/maxresdefault.jpg')">
                                <div class="overlay"></div>
                                <div class="widget-item">
                                    <h4>The Witcher 3: Wild Hunt</h4>
                                    <span class="meta">Released: May 18, 2015</span>

                                    <h5>Platforms</h5>
                                    <a href="#"><span class="badge badge-xbox-one">Xbox One</span></a>
                                    <a href="#"><span class="badge badge-ps4">Ps4</span></a>
                                    <a href="#"><span class="badge badge-steam">Steam</span></a>

                                    <h5>Developed</h5>
                                    <ul>
                                        <li><a href="#">CD Projekt Red Studio</a></li>
                                        <li><a href="#">CD Projekt S.A.</a></li>
                                    </ul>

                                    <h5>Published</h5>
                                    <ul>
                                        <li><a href="#">Warner Bros. Interactive Entertainment</a></li>
                                        <li><a href="#">CD Projekt RED S.A.</a></li>
                                        <li><a href="#">Bandai Games</a></li>
                                    </ul>

                                    <h5>Time to beat</h5>
                                    <ul>
                                        <li><a href="#">180 hours</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <section class="bg-secondary p-t-15 p-b-5 p-x-15">
                        <div class="owl-carousel owl-videos">
                            <div class="card card-video">
                                <div class="card-img">
                                    <a data-lightbox href="https://i1.ytimg.com/vi/GaERL8Nrl9k/mqdefault.jpg">
                                        <img src="https://i1.ytimg.com/vi/GaERL8Nrl9k/mqdefault.jpg"
                                             alt="Tom Clancy's Ghost Recon: Wildlands">
                                    </a>
                                    <div class="card-meta">
                                        <span>4:32</span>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title"><a href="video-post.html">Tom Clancy's Ghost Recon:
                                            Wildlands</a></h4>
                                    <div class="card-meta">
                                        <span><i class="fa fa-clock-o"></i> 2 hours ago</span>
                                        <span>423 views</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-video">
                                <div class="card-img">
                                    <a data-lightbox href="https://i1.ytimg.com/vi/mW4LMCtoIkg/mqdefault.jpg">
                                        <img src="https://i1.ytimg.com/vi/mW4LMCtoIkg/mqdefault.jpg"
                                             class="card-img-top" alt="Anthem Official Gameplay Reveal">
                                    </a>
                                    <div class="card-meta">
                                        <span>6:46</span>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title"><a href="video-post.html">Anthem Official Gameplay Reveal</a>
                                    </h4>
                                    <div class="card-meta">
                                        <span><i class="fa fa-clock-o"></i> 2 weeks ago</span>
                                        <span>447 views</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-video">
                                <div class="card-img">
                                    <a data-lightbox href="https://i1.ytimg.com/vi/-PohBqV_i7s/mqdefault.jpg">
                                        <img src="https://i1.ytimg.com/vi/-PohBqV_i7s/mqdefault.jpg"
                                             class="card-img-top" alt="Shadow of War Gameplay Walkthrough">
                                    </a>
                                    <div class="card-meta">
                                        <span>9:58</span>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title"><a href="video-post.html">Shadow of War Gameplay
                                            Walkthrough</a></h4>
                                    <div class="card-meta">
                                        <span><i class="fa fa-clock-o"></i> March 10, 2017</span>
                                        <span>914 views</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-video">
                                <div class="card-img">
                                    <a data-lightbox href="https://i1.ytimg.com/vi/feqIj5PaqCQ/mqdefault.jpg">
                                        <img src="https://i1.ytimg.com/vi/feqIj5PaqCQ/mqdefault.jpg"
                                             class="card-img-top" alt="Call of Duty WW2 Multiplayer Gameplay">
                                    </a>
                                    <div class="card-meta">
                                        <span>4:32</span>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title"><a href="video-post.html">Call of Duty WW2 Multiplayer
                                            Gameplay</a></h4>
                                    <div class="card-meta">
                                        <span><i class="fa fa-clock-o"></i> 3 days ago</span>
                                        <span>423 views</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-video">
                                <div class="card-img">
                                    <a data-lightbox href="https://i.ytimg.com/vi/N1NsF9c90f0/mqdefault.jpg">
                                        <img src="https://i.ytimg.com/vi/N1NsF9c90f0/mqdefault.jpg"
                                             alt="Final Fantasy VII Remake">
                                    </a>
                                    <div class="card-meta">
                                        <span>3:05</span>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title"><a href="video-post.html">Final Fantasy VII Remake</a></h4>
                                    <div class="card-meta">
                                        <span><i class="fa fa-clock-o"></i> 2 days ago</span>
                                        <span>589 views</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-video">
                                <div class="card-img">
                                    <a data-lightbox href="https://i.ytimg.com/vi/lQXpDL3SNWQ/mqdefault.jpg">
                                        <img src="https://i.ytimg.com/vi/lQXpDL3SNWQ/mqdefault.jpg" class="card-img-top"
                                             alt="Spider-Man Official 4K Trailer">
                                    </a>
                                    <div class="card-meta">
                                        <span>3:07</span>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title"><a href="video-post.html">Spider-Man Official 4K Trailer</a>
                                    </h4>
                                    <div class="card-meta">
                                        <span><i class="fa fa-clock-o"></i> 2 weeks ago</span>
                                        <span>1798 views</span>
                                    </div>
                                </div>
                            </div>

                            <div class="card card-video">
                                <div class="card-img">
                                    <a data-lightbox href="https://i1.ytimg.com/vi/9EzRBzdf--g/mqdefault.jpg">
                                        <img src="https://i1.ytimg.com/vi/9EzRBzdf--g/mqdefault.jpg"
                                             alt="METRO EXODUS Gameplay Trailer">
                                    </a>
                                    <div class="card-meta">
                                        <span>1:24</span>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title"><a href="video-post.html">Wolfenstein II Gameplay Trailer</a>
                                    </h4>
                                    <div class="card-meta">
                                        <span><i class="fa fa-clock-o"></i> July 16, 2017</span>
                                        <span>7330 views</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection