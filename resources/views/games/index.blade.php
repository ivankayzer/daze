@extends('layouts.app')

@section('styles')
    <link href="{{ asset('plugins/owl-carousel/css/owl.carousel.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{ asset('plugins/easypiechart/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('plugins/easypiechart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('plugins/owl-carousel/js/owl.carousel.min.js') }}"></script>
    <script>
        $('.owl-list').owlCarousel({
            margin: 25,
            nav: true,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 2
                },
                701: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        });
    </script>
    <script>
        (function($) {
            "use strict";
            // easyPieChart
            $('.chart').easyPieChart({
                barColor: '#5eb404',
                trackColor: '#e3e3e3',
                easing: 'easeOutBounce',
                onStep: function(from, to, percent) {
                    $(this.el).find('span').text(Math.round(percent));
                }
            });
        })(jQuery);
    </script>
@endsection

@section('content')
    <section class="hero hero-game" style="background-image: url('{{ $promo->getCover()->get1080p() }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="hero-block">
                <div class="hero-left">
                    <h2 class="hero-title">{{ $promo->getName() }}</h2>
                    <p>{{ $promo->getShortSummary() }}</p>
                    <a class="btn btn-primary btn-lg btn-shadow btn-rounded btn-effect" href="{{ route('games.show') }}" role="button">Read More</a>
                </div>
                <div class="hero-right">
                    <div class="hero-review">
                        <span>IGDB</span>
                        <div class="chart easypiechart" data-percent="{{ $promo->getIGDBRating() }}" data-scale-color="#e3e3e3"><span>{{ $promo->getIGDBRating() }}</span>%</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-secondary no-border-bottom p-y-80">
        <div class="container">
            <div class="heading">
                <i class="fa fa-star"></i>
                <h2>Trending now</h2>
            </div>
            <div class="owl-carousel owl-list">
                @foreach($trending->getGames() as $game)
                    @include('cards.games.trending', ['game' => $game])
                @endforeach
            </div>
        </div>
    </section>

    <section class="p-t-30">
        <div class="container">
            <div class="toolbar-custom">
                <form method="get" action="{{ route('search.index') }}" class="float-left cold-12 col-sm-12 p-l-0 p-r-10">
                    <div class="form-group input-icon-right">
                        <i class="fa fa-search"></i>
                        <input type="text" name="term" class="form-control search-game" placeholder="Search Game...">
                    </div>
                </form>
            </div>

            <div class="row">
                @foreach($list->getGames() as $game)
                    @include('cards.games.games', ['game' => $game])
                @endforeach
            </div>

            <div class="pagination-results m-t-0">
                <span>Showing {{ $list->getRange() }} of {{ $list->getPaginator()->total() }} games</span>
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="page-item disabled"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="separate"><span>...</span></li>
                        <li class="page-item"><a class="page-link" href="#">25</a></li>
                        <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
@endsection