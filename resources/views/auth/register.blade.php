@extends('layouts.app')

@section('content')
    <section class="bg-image bg-image-sm" style="background-image: url('https://img.youtube.com/vi/BhTkoDVgF6s/maxresdefault.jpg'); min-height: 797px;">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-8 col-md-4 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><i class="fa fa-user-plus"></i> Register a new account</h4>
                        </div>
                        <div class="card-block">
                            <form action="{{ route('register') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-user"></i>
                                    <input type="text" name="name" class="form-control form-control-secondary" placeholder="Username">
                                </div>
                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-envelope"></i>
                                    <input type="email" name="email" class="form-control form-control-secondary" placeholder="Email Address">
                                </div>
                                <div class="divider"><span>Security</span></div>
                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-lock"></i>
                                    <input type="password" name="password" class="form-control form-control-secondary" placeholder="Password">
                                </div>
                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-unlock"></i>
                                    <input type="password" name="password_confirmation" class="form-control form-control-secondary" placeholder="Repeat Password">
                                </div>
                                <button type="submit" class="btn btn-primary m-t-10 btn-block">Complete Registration</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
