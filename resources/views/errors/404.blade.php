@extends('layouts.app')

@section('content')
    <section class="bg-image bg-image-sm error-404" style="background-image: url('https://img.youtube.com/vi/y3Cpetu4ke4/maxresdefault.jpg'); min-height: 797px;">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="heading">
                        <h2>404</h2>
                    </div>
                    <p>Sorry, but the page you requested could not be found.</p>
                    <div class="m-t-50">
                        <a href="{{ route('welcome') }}" class="btn btn-primary btn-effect btn-shadow btn-rounded btn-lg">Back to home</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection