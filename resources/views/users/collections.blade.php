<section class="bg-secondary no-border-bottom p-y-50">
    <div class="container">
        <div id="accordion" class="accordion" role="tablist">
            <div class="widget" role="tab" id="headingOne">
                <h2 class="widget-title active" data-toggle="collapse" href="#collapseOne" aria-expanded="true"
                    aria-controls="collapseOne">Best of 2018</h2>
            </div>
            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne"
                 data-parent="#accordion">
                <div class="row">
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Uncharted 4</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Hitman: Absolution</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Last of us: Remastered</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Bioshock: Infinite</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Grand Theft Auto: 5</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Dayz</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title">
                                <a href="{{ route('games.show') }}">Liberty City</a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget" role="tab" id="headingTwo">
                <h2 class="widget-title active" data-toggle="collapse" href="#collapseTwo" aria-expanded="true"
                    aria-controls="collapseOne">E3</h2>
            </div>
            <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo"
                 data-parent="#accordion">
                <div class="row">
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Uncharted 4</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Hitman: Absolution</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Last of us: Remastered</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Bioshock: Infinite</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Grand Theft Auto: 5</a></h4>
                        </div>
                    </div>
                    <div class="card card-review col-lg-2 m-b-25">
                        <div class="dropdown float-right card-collection-dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Move to another collection</a>
                                <a class="dropdown-item" href="#">Delete from collection</a>
                            </div>
                        </div>
                        <a class="card-img m-b-20" href="{{ route('games.show') }}">
                            <img src="{{ asset('img/review/review-6.jpg') }}" alt="">
                        </a>
                        <div class="card-block">
                            <h4 class="card-title"><a href="{{ route('games.show') }}">Dayz</a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('script')
    @parent

    <script>
        $(document).ready(function () {
            $('#accordion').on('hide.bs.collapse', function (e) {
                $(this).find('[href="#' + e.target.id + '"]').removeClass('active');
            });

            $('#accordion').on('show.bs.collapse', function (e) {
                $(this).find('[href="#' + e.target.id + '"]').addClass('active');
            });
        });
    </script>
@endsection