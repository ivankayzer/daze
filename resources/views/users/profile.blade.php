@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('plugins/sticky/jquery.sticky.js') }}"></script>
    <script src="{{ asset('plugins/lightbox/lightbox.js') }}"></script>
    <script>
        (function($) {
            "use strict";
            $('[data-lightbox]').lightbox({
                disqus: 'gameforestyakuzieu'
            });
        })(jQuery);
    </script>
@endsection

@section('content')
    <section class="hero hero-profile" style="background-image: url('https://img.youtube.com/vi/D3pYbbA1kfk/maxresdefault.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="hero-block">
                <h5>Nathan Drake</h5>
                <a class="btn btn-primary btn-sm btn-shadow btn-rounded btn-icon btn-add" href="#" data-toggle="tooltip" title="Follow" role="button"><i class="fa fa-user-plus"></i></a>
            </div>
        </div>
    </section>

    <section class="toolbar toolbar-profile" data-fixed="true">
        <div class="container">
            <div class="profile-avatar">
                <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}" alt=""></a>
                <div class="sticky">
                    <a href="#"><img src="{{ asset('img/user/avatar-sm.jpg') }}" alt=""></a>
                    <div class="profile-info">
                        <h5>Nathan Drake</h5>
                        <span>@nathan</span>
                    </div>
                </div>
            </div>
            <ul class="toolbar-nav hidden-md-down">
                <li class="active"><a href="{{ route('users.profile.timeline') }}">Timeline</a></li>
                <li><a href="{{ route('users.profile.collections') }}">Collections (2)</a></li>
                <li><a href="#">Following (628)</a></li>
                <li><a href="#">Followers (14)</a></li>
                <li><a href="{{ route('users.profile.images') }}">Images (23)</a></li>
                <li><a href="#">Saved News (13)</a></li>
            </ul>
        </div>
    </section>

    @include('users.' . $tab)
@endsection