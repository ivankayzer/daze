<section>
    <div class="container">
        <div class="row row-3 figure-effect p-y-50">
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/JbgJA-r86HQ/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/JbgJA-r86HQ/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/Q0cWcCJUj2Y/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/Q0cWcCJUj2Y/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/PZmPm4teJdo/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/PZmPm4teJdo/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/AA2YRRCPqCk/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/AA2YRRCPqCk/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/FpfZOHzOoZM/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/FpfZOHzOoZM/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/aPXdxipSlCw/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/aPXdxipSlCw/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/d4BC48aem60/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/d4BC48aem60/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/xM__xYONwvs/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/xM__xYONwvs/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/a_uuQyUGYH4/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/a_uuQyUGYH4/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/D3pYbbA1kfk/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/D3pYbbA1kfk/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/zIjQCu8y2EE/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/zIjQCu8y2EE/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/IDnAfWlAoO0/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/IDnAfWlAoO0/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/mJOdi7UemSo/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/mJOdi7UemSo/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/3byajfBLot0/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/3byajfBLot0/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <figure>
                    <div class="figure-img">
                        <a href="https://i.ytimg.com/vi/-bGI9jobudM/maxresdefault.jpg" data-lightbox='{"disqus": true, "gallery": "uncharted"}'>
                            <img src="https://i.ytimg.com/vi/-bGI9jobudM/maxresdefault.jpg" alt="">
                        </a>
                        
                    </div>
                </figure>
            </div>
        </div>

        <div class="text-center m-t-30"><a class="btn btn-primary btn-shadow btn-rounded btn-effect btn-lg" href="#">Show More</a></div>
    </div>
</section>