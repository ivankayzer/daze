<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Repositories\GameRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * SearchController constructor.
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function search(SearchRequest $request)
    {
        $term = $request->get('term');

        return view('search', [
            'results' => $this->gameRepository->search($term),
            'term' => $term
        ]);
    }
}
