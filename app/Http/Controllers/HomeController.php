<?php

namespace App\Http\Controllers;

use App\Repositories\GameRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * HomeController constructor.
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function index()
    {
        return view('welcome', [
            'recent' => $this->gameRepository->getRecent()
        ]);
    }
}
