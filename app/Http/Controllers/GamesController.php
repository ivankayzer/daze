<?php

namespace App\Http\Controllers;

use App\Repositories\GameRepository;
use Illuminate\Http\Request;

class GamesController extends Controller
{
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * GamesController constructor.
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function index()
    {
        return view('games.index', [
            'promo' => $this->gameRepository->getPromo(),
            'trending' => $this->gameRepository->getTrending(),
            'list' => $this->gameRepository->getPaginatedList()
        ]);
    }

    public function show()
    {
        return view('games.show');
    }
}
