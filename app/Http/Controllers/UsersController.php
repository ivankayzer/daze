<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UsersController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->middleware(['auth']);
    }

    public function timeline($username = null)
    {
        return view('users.profile', [
            'tab' => 'timeline',
        ]);
    }

    public function images()
    {
        return view('users.profile', [
            'tab' => 'images'
        ]);
    }

    public function collections()
    {
        return view('users.profile', [
            'tab' => 'collections'
        ]);
    }

    public function follow(int $profileId)
    {
        $user = $this->userRepository->get($profileId);

        if(!$user) {
            return redirect()->back();
        }

        $user->followers()->attach(auth()->user()->id);
        return redirect()->back();
    }

    public function unfollow(int $profileId)
    {
        $user = $this->userRepository->get($profileId);

        if(!$user) {
            return redirect()->back();
        }

        $user->followers()->detach(auth()->user()->id);

        return redirect()->back();
    }
}
