<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;

class ProfileComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        if (!auth()->check()) {
            $view->with('ownProfile', false);

            return;
        }

        $username = request()->route()->parameter('username');

        $profile = $this->users->getByUsername($username ?? auth()->user()->name);

        if (is_null($username) || $username === auth()->user()->name) {
            $view->with('ownProfile', true);
        }

        $view->with('profile', $profile);
    }
}