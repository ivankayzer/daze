<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /**
     * @param int $id
     * @return User|null
     */
    public function get(int $id): ?User
    {
        return User::where('id', $id)->first();
    }

    /**
     * @param string $username
     * @return User|null
     */
    public function getByUsername(string $username): ?User
    {
        return User::where('name', $username)->first();
    }
}