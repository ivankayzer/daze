<?php

namespace App\Repositories;

use App\Game;
use App\Jobs\SaveGame;
use App\Services\GameCollection;
use App\Services\GameObject;
use App\Services\IGDBGameCollection;
use App\Services\PaginatedGameCollection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use IGDB;

class GameRepository
{
    public function getRecent($count = 6)
    {
        return new GameCollection(Game::orderBy('created_at')->limit($count)->get());
    }

    public function save($game)
    {
        $game = collect($game);

        $model = new Game;

        $model->fill($game->only([
            'name', 'slug', 'url', 'summary', 'rating_count', 'developers', 'publishers', 'release_dates',
            'game_engines', 'player_perspectives', 'game_modes', 'themes', 'genres', 'dlcs', 'platforms', 'games',
            'alternative_names', 'screenshots', 'websites', 'first_release_date'
            ])->toArray());

        $model->game_id = $game->get('id');
        $model->rating = $game->get('total_rating');
        $model->cover = $game->get('cover') ? $game->get('cover')->cloudinary_id : '';

        $model->save();

        return new GameObject($model);
    }

    public function get($id)
    {
        return Cache::rememberForever("games:{$id}", function () use ($id) {
            $game = Game::where('game_id', $id)->first();

            if ($game) {
                return new GameObject($game);
            }

            return $this->save(IGDB::getGame($id));
        });
    }

    public function search($term)
    {
        return Cache::rememberForever("search:{$term}", function () use ($term) {
            return $this->saveMultiple(IGDB::searchGames($term, ['*'], 20));
        });
    }

    protected function saveMultiple($search)
    {
        foreach ($search as $game) {
            dispatch(new SaveGame($game, app(GameRepository::class)));
        }

        return new IGDBGameCollection($search);
    }

    public function getTrending($count = 7)
    {
        return new GameCollection(
            Game::orderBy('first_release_date', 'desc')
                ->orderBy('rating', 'desc')
                ->whereNotNull('rating')
                ->where('first_release_date', '>', Carbon::now()->setDate(Carbon::now()->year - 1, 1, 1)->timestamp * 1000)
                ->limit($count)
                ->get()
        );
    }

    public function getPaginatedList($count = 9)
    {
        return new PaginatedGameCollection(Game::paginate($count));
    }

    public function getPromo()
    {
        return new GameObject(
            Game::orderBy('rating', 'desc')
                ->where('first_release_date', '>', Carbon::now()->startOfYear()->timestamp * 1000)
                ->whereNotNull('rating')
                ->first()
        );
    }
}