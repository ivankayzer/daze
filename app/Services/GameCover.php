<?php

namespace App\Services;

class GameCover
{
    protected $link;

    /**
     * GameCover constructor.
     * @param $link
     */
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * @param string $size
     * @return mixed
     */
    protected function getLink($size = 'cover_small')
    {
        return "https://images.igdb.com/igdb/image/upload/t_{$size}/{$this->link}.jpg";
    }

    // 90 x 128
    public function getCoverSmall()
    {
        return $this->getLink('cover_small');
    }

    // 227 x 320
    public function getCoverBig()
    {
        return $this->getLink('cover_big');
    }

    // 284 x 160
    public function getLogoMed()
    {
        return $this->getLink('logo_med');
    }

    // 	569 x 320
    public function getScreenshotMed()
    {
        return $this->getLink('screenshot_med');
    }

    // 889 x 500
    public function getScreenshotBig()
    {
        return $this->getLink('screenshot_big');
    }

    // 1280 x 720
    public function getScreenshotHuge()
    {
        return $this->getLink('screenshot_huge');
    }

    // 	90 x 90
    public function getThumb()
    {
        return $this->getLink('thumb');
    }

    // 35 x 35
    public function getMicro()
    {
        return $this->getLink('micro');
    }

    // 1280 x 720
    public function get720p()
    {
        return $this->getLink('720p');
    }

    // 1920 x 1080
    public function get1080p()
    {
        return $this->getLink('1080p');
    }
}