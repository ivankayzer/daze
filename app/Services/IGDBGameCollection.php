<?php

namespace App\Services;

class IGDBGameCollection extends GameCollection
{
    /**
     * IGDBGameCollection constructor.
     * @param $games
     */
    public function __construct($games)
    {
        $this->count = count($games);

        foreach ($games as $game) {
            $this->games[] = new IGDBGameObject($game);
        }
    }
}