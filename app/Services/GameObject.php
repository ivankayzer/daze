<?php

namespace App\Services;

class GameObject extends IGDBGameObject
{
    protected $rating;

    public function __construct($game)
    {
        parent::__construct($game);

        $this->rating = data_get($game, 'rating');
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return round($this->rating / 10, 1);
    }

    public function getIGDBRating()
    {
        return (int) round($this->rating);
    }
}