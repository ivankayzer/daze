<?php

namespace App\Services;

use App\Services\GameObject;
use Illuminate\Support\Collection;

class GameCollection
{
    protected $count;

    protected $games;

    /**
     * GameCollection constructor.
     * @param Collection|array $games
     */
    public function __construct($games)
    {
        $this->count = count($games);

        foreach ($games as $game) {
            $this->games[] = new GameObject($game);
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return mixed
     */
    public function getGames()
    {
        return collect($this->games);
    }

    /**
     * @param mixed $games
     * @return GameCollection
     */
    public function setGames($games)
    {
        $this->games = $games;

        $this->count = count($games);

        return $this;
    }
}