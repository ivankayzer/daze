<?php

namespace App\Services;

class IGDBQueryBuilder
{
    private $url;
    private $type = 'games';
    private $query = '';
    private $options = [];

    public function __construct()
    {
        $this->url = config('services.igdb.url');
    }

    public function type($type)
    {
        $this->type = $type;

        return $this;
    }

    public function query($query)
    {
        $this->query = $query;

        return $this;
    }

    public function options($options = [])
    {
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    public function limit($limit)
    {
        $this->options['limit'] = $limit;

        return $this;
    }

    public function getUrl()
    {
        return $this->url.$this->type.'/'.$this->query.'?'.http_build_query($this->options);
    }

    public function get()
    {
        return $this->getUrl();
    }

    public function getCacheId()
    {
        return $this->type.':'.$this->query;
    }

    public function reset()
    {
        $this->type = 'games';
        $this->options = [];
        $this->query = '';

        return $this;
    }
}
