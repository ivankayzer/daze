<?php

namespace App\Services;

use Carbon\Carbon;

class IGDBGameObject
{
    protected $id;

    protected $name;

    protected $slug;

    protected $summary;

    protected $cover;

    protected $releaseDate;

    /**
     * IGDBGameObject constructor.
     * @param $game
     */
    public function __construct($game)
    {
        $this->id = data_get($game, 'id');
        $this->name = data_get($game, 'name');
        $this->slug = data_get($game, 'slug');
        $this->summary = data_get($game, 'summary');
        $this->releaseDate = data_get($game, 'first_release_date');

        $cover = data_get($game, 'cover');
        $cover = is_object($cover) ? $cover->cloudinary_id : $cover;

        $this->cover = new GameCover($cover);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getSummary()
    {
        return $this->summary;
    }

    public function getShortSummary($length = 120)
    {
        return str_limit($this->getSummary(), $length);
    }

    /**
     * @return GameCover
     */
    public function getCover(): GameCover
    {
        return $this->cover;
    }

    /**
     * @return mixed
     */
    public function getReleaseDate()
    {
        if (!$this->releaseDate) {
            return '';
        }

        return Carbon::createFromTimestamp($this->releaseDate / 1000)->toFormattedDateString();
    }

    public function getLink()
    {
        return route('games.show', ['id' => $this->getId(), 'slug' => $this->getSlug()]);
    }
}