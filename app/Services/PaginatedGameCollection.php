<?php

namespace App\Services;

use Illuminate\Pagination\LengthAwarePaginator;

class PaginatedGameCollection
{
    protected $games;

    protected $paginator;

    /**
     * GameCollection constructor.
     * @param LengthAwarePaginator $paginator
     */
    public function __construct(LengthAwarePaginator $paginator)
    {
        foreach ($paginator->getCollection() as $game) {
            $this->games[] = new GameObject($game);
        }

        $this->paginator = $paginator;
    }

    /**
     * @return mixed
     */
    public function getGames()
    {
        return collect($this->games);
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getPaginator(): LengthAwarePaginator
    {
        return $this->paginator;
    }

    public function getRange()
    {
        $current = ($this->getPaginator()->currentPage() - 1) * $this->getPaginator()->perPage() + 1;
        return $current . ' to ' . ($current + $this->getPaginator()->perPage() - 1);
    }
}