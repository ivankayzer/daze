<?php

namespace App\Services;

class Timeline
{
    public function __construct()
    {
        if (!auth()->check()) {
            return false;
        }

        auth()->user()->events()->create([
            'event' => class_basename($this)
        ]);
    }
}