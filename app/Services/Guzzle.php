<?php

namespace App\Services;

//use GuzzleHttp\Client as GuzzleVendor;

/**
 * Class Guzzle.
 */
class Guzzle
{
    /**
     * @var GuzzleVendor
     */
    private $client;

    /**
     * @var array
     */
    private $headers = [
        'headers' => [],
    ];

    /**
     * @var \GuzzleHttp\Psr7\Response
     */
    private $response;

//    /**
//     * Guzzle constructor.
//     *
//     * @param GuzzleVendor $client
//     */
//    public function __construct(GuzzleVendor $client)
//    {
//        $this->client = $client;
//    }

    /**
     * @param $url
     * @param array $options
     *
     * @return $this
     */
    public function get($url, $options = [])
    {
        $this->response = $this->client->get($url, array_merge($options, $this->headers));

        return $this;
    }

    public function getOne($url, $options = [])
    {
        return $this->get($url, $options)->decode()[0];
    }

    /**
     * @param $headers
     *
     * @return $this
     */
    public function setHeaders(array $headers)
    {
        $this->headers = array_merge($this->headers, ['headers' => $headers]);

        return $this;
    }

    /**
     * @return mixed
     */
    public function decode()
    {
        return json_decode($this->response->getBody()->getContents(), true);
    }
}
