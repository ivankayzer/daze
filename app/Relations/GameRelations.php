<?php

namespace App\Relations;

class GameRelations
{
    public function convert($data, $relation)
    {
        /** @var AbstractRelation $relation */
        return (new $relation)->convert($data);
    }
}