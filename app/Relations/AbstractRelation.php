<?php
namespace App\Relations;

abstract class AbstractRelation
{
    protected $data;

    public function getType()
    {
        return snake_case(class_basename($this));
    }

    public function convert($toConvert)
    {
        return $toConvert;
    }
}