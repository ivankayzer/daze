<?php

namespace App\Relations\Relations;

use App\Relations\AbstractRelation;
use App\Relations\Contracts\ModelSource;
use App\Relations\GameRelations;

class Websites extends AbstractRelation implements ModelSource
{
    public function convert($data)
    {
        return collect($data)->map(function ($datum) {
            return [
                'category' => (new GameRelations)->convert($datum->category, WebsiteCategory::class),
                'url' => $datum->url,
            ];
        })->toArray();
    }
}