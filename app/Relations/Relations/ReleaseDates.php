<?php

namespace App\Relations\Relations;

use App\Relations\Contracts\JSONSource;
use Illuminate\Support\Facades\Storage;

class ReleaseDates implements JSONSource
{
    /**
     * @param $toConvert
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function convert($toConvert)
    {
        $platforms = json_decode(Storage::disk('relations')->get('platforms.json'));

        $platformsList = [];
        foreach ($platforms as $platform) {
            $platformsList[$platform->id] = $platform->name;
        }

        return array_map(function ($date) use ($platformsList) {
            return [
                'date' => $date->human,
                'platform' => isset($platformsList[$date->platform]) ? $platformsList[$date->platform] : null
            ];
        }, $toConvert);
    }
}