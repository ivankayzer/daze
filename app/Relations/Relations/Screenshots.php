<?php

namespace App\Relations\Relations;

use App\Relations\AbstractRelation;
use App\Relations\Contracts\ModelSource;

class Screenshots extends AbstractRelation implements ModelSource
{
    public function convert($gameData)
    {
        return collect($gameData)->map(function ($datum) {
            return $datum->cloudinary_id;
        })->toArray();
    }
}