<?php

namespace App\Relations\Relations;

use App\Relations\Contracts\ArraySource;
use App\Relations\Convertable;

class WebsiteCategory extends Convertable implements ArraySource
{
    protected $data = [
        1 => 'official',
        2 => 'wikia',
        3 => 'wikipedia',
        4 => 'facebook',
        5 => 'twitter',
        6 => 'twitch',
        8 => 'instagram',
        9 => 'youtube',
        10 => 'iphone',
        11 => 'ipad',
        12 => 'android',
        13 => 'steam',
        14 => 'Reddit',
    ];
}