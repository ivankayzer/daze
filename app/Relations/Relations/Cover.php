<?php

namespace App\Relations\Relations;

class Cover
{
    public function convert($data)
    {
        return "https://images.igdb.com/igdb/image/upload/screenshot_med/" . $data->cloudinary_id . ".jpg";
    }
}