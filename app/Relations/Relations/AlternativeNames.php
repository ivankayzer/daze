<?php

namespace App\Relations\Relations;

use App\Relations\AbstractRelation;
use App\Relations\Contracts\ModelSource;

class AlternativeNames extends AbstractRelation implements ModelSource
{
    public function convert($data)
    {
        return collect($data)->map(function ($datum) {
            return [
                'name' => data_get($datum, 'name'),
                'comment' => data_get($datum, 'comment'),
            ];
        })->toArray();
    }
}