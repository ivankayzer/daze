<?php

namespace App\Relations;

use App\Relations\Contracts\ArraySource;
use App\Relations\Contracts\JSONSource;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

abstract class Convertable extends AbstractRelation
{
    /**
     * @param $toConvert
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */

    public function convert($toConvert)
    {
        if ($this instanceof JSONSource) {
            $this->data = json_decode(Storage::disk('relations')->get($this->getType() . '.json'));
        }

        if ($this instanceof ArraySource) {
            $this->data = collect($this->data)->map(function ($value, $index) {
                return (object) ['id' => $index, 'name' => $value];
            })->toArray();
        }

        if (!is_array($toConvert)) {
            $toConvert = [$toConvert];
        }

        $results = [];

        foreach ($this->data as $datum) {
            if (in_array($datum->id, $toConvert)) {
                $results[$datum->id] = $datum->name;
            }
        }

        foreach ($toConvert as $item) {
            if (!in_array($item, array_keys($results))) {
                Log::warn('Missing value: ' . $item . ' in ' . class_basename($this));
            }
        }

        return $results;
    }
}