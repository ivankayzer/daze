<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Game extends Model
{
    protected $guarded = [];

    protected $casts = [
        'franchise' => 'array',
        'games' => 'array',
        'developers' => 'array',
        'publishers' => 'array',
        'game_engines' => 'array',
        'player_perspectives' => 'array',
        'release_dates' => 'array',
        'game_modes' => 'array',
        'themes' => 'array',
        'genres' => 'array',
        'dlcs' => 'array',
        'platforms' => 'array',
        'alternative_names' => 'array',
        'screenshots' => 'array',
        'websites' => 'array',
    ];

    public function collections()
    {
        $this->belongsToMany(Collection::class);
    }
}
