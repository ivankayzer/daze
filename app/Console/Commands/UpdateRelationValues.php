<?php

namespace App\Console\Commands;

use App\Relations\AbstractRelation;
use App\Relations\Relations\Companies;
use App\Relations\Relations\Franchises;
use App\Relations\Relations\GameEngines;
use App\Relations\Relations\GameModes;
use App\Relations\Relations\Genres;
use App\Relations\Relations\Platforms;
use App\Relations\Relations\PlayerPerspectives;
use App\Relations\Relations\Themes;
use App\Services\Guzzle;
use App\Services\IGDBQueryBuilder;
use Illuminate\Console\Command;

class UpdateRelationValues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relations:update';

    protected $relations = [
        Themes::class,
        GameEngines::class,
        GameModes::class,
        Genres::class,
        Platforms::class,
        PlayerPerspectives::class,
        Companies::class, // -> needs scroll API
        Franchises::class
    ];

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update igdb games relations values';

    /**
     * @var Guzzle
     */
    private $guzzle;
    /**
     * @var IGDBQueryBuilder
     */
    private $builder;

    private $limit = 50;

    /**
     * Create a new command instance.
     *
     * @param Guzzle $guzzle
     * @param IGDBQueryBuilder $builder
     */
    public function __construct(Guzzle $guzzle, IGDBQueryBuilder $builder)
    {
        parent::__construct();

        $this->guzzle = $guzzle->setHeaders(['user-key' => config('services.igdb.key')]);
        $this->builder = $builder;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->relations as $relation) {
            /** @var AbstractRelation $relation */
            $relation = new $relation;

            $this->info('Started processing: ' . $relation->getType());

            $count = array_get($this->guzzle->get($this->builder->reset()->type($relation->getType())->query('count')->get())->decode(), 'count');

            /** Scroll API needed */
            if ($count > 9950) {
                $count = 9950;
            }

            $this->info($count . ' items to update');

            $bar = $this->output->createProgressBar($count);

            $links = [];

            for ($i = 0; $i < $count; $i += $this->limit) {
                $links[] = $this->builder->reset()->type($relation->getType())->limit($this->limit)->options(['fields' => 'id,name', 'offset' => $i])->get();
            }

            $indexes = [];

            foreach ($links as $link) {
                $indexes[] = $this->guzzle->get($link)->decode();
                $bar->advance($this->limit);
            }

            $bar->finish();

            $indexes = collect($indexes)->flatten(1);

            \Storage::disk('relations')->delete($relation->getType() . '.json');
            \Storage::disk('relations')->put($relation->getType() . '.json', $indexes);

            $this->info(PHP_EOL . 'Finished processing: ' . $relation->getType());

            $this->line('');
            $this->line('============================================');
            $this->line('');
        }
    }
}
