<?php

namespace App\Jobs;

use App\Game;
use App\Repositories\GameRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SaveGame implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $game;
    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * Create a new job instance.
     *
     * @param $game
     * @param GameRepository $gameRepository
     */
    public function __construct($game, GameRepository $gameRepository)
    {
        $this->game = $game;
        $this->gameRepository = $gameRepository;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $game = Game::where('game_id', $this->game->id)->first();

        if ($game) {
            return;
        }

        $this->gameRepository->save($this->game);
    }
}
