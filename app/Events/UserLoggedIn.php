<?php

namespace App\Events;

use App\Services\Timeline;
use App\User;
use Illuminate\Foundation\Events\Dispatchable;

class UserLoggedIn extends Timeline
{
    use Dispatchable;

    /**
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;
    }
}
