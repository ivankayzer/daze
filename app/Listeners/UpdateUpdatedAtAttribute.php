<?php

namespace App\Listeners;

use App\Events\UserLoggedIn;

class UpdateUpdatedAtAttribute
{
    /**
     * Handle the event.
     *
     * @param  UserLoggedIn  $event
     * @return void
     */
    public function handle(UserLoggedIn $event)
    {
        $event->user->touch();
    }
}
