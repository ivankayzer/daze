<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/search', 'SearchController@search')->name('search.index');

Auth::routes();

Route::get('/news', 'NewsController@index')->name('news.index');
Route::get('/news/show', 'NewsController@show')->name('news.show');
Route::get('/games', 'GamesController@index')->name('games.index');
Route::get('/games/show', 'GamesController@show')->name('games.show');

Route::group(['prefix' => 'profile'], function () {
    Route::get('{username?}', 'UsersController@timeline')->name('users.profile.timeline');
    Route::get('images', 'UsersController@images')->name('users.profile.images');
    Route::get('collections', 'UsersController@collections')->name('users.profile.collections');
    Route::post('{profileId}/follow', 'UsersController@follow')->name('users.follow');
    Route::post('{profileId}/unfollow', 'UsersController@unfollow')->name('users.unfollow');
});
